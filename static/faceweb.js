function img_update(){
    document.getElementById("sample_jpg").src = "/static/sample.jpg?time=" + Date.now();
}

function svg_clear(elem){
    while (elem.firstChild){
	elem.removeChild(elem.firstChild);
    }
}

function svg_line(elem, parts){
    line = document.createElementNS('http://www.w3.org/2000/svg','line');
    line.setAttribute('x1', parts['x1']);
    line.setAttribute('y1', parts['y1']);
    line.setAttribute('x2', parts['x2']);
    line.setAttribute('y2', parts['y2']);
    line.setAttribute('stroke', 'red');
    line.setAttribute('stroke-width', '3');
    elem.appendChild(line);
}

function svg_rect(elem, parts){
    line = document.createElementNS('http://www.w3.org/2000/svg','rect');
    line.setAttribute('x',      parts['x']);
    line.setAttribute('y',      parts['y']);
    line.setAttribute('width',  parts['width']);
    line.setAttribute('height', parts['height']);
    line.setAttribute('stroke', 'black');
    line.setAttribute('stroke-width', '3');
    elem.appendChild(line);
}

function svg_update(faces){
    console.log(faces);
    elem = document.getElementById('face_svg');
    svg_clear(elem);
    faces.forEach((face, fidx, fary)=>{
	face.forEach((parts, pidx, ary)=>{
	    if (parts['type'] == 'line'){
		svg_line(elem, parts);
	    } else if (parts['type'] == 'rect'){
		svg_rect(elem, parts);
	    }
	});
    });
}

function ws_update(){
    const threshold = 3;
    const socket = new WebSocket('ws://' + document.domain + ':' + '5001' + '/face');
    socket.addEventListener('open', function (event) {
	socket.send('Hello Server!');
    });
    socket.addEventListener('message', function (event) {
	json = JSON.parse(event.data);
	console.log(json);
	svg_update(json['svg']);
	if (json['orientation'] < -threshold){
	    document.getElementById("btnR").click();
	} else if(json['orientation'] > threshold){
	    document.getElementById("btnL").click();
	}
    });
    socket.addEventListener('close', function (event) {
	ws_update();
    });
    socket.addEventListener('error', function (event) {
	console.error('Socket encountered error: ', event.message, 'Closing socket');
    });
}

function init(){
    setInterval(img_update, 1000);
    ws_update();
}

window.addEventListener('load', init);
