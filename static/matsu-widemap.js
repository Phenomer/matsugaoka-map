// 鶴岡市デモ用

(function (){
    function init() {
	var image = '/static/matsugaoka02.jpg';	// 画像ファイル名
	var x = 8450, y = 1710;		// 画像の幅と高さ
	var leftBottom = [0, 0], rightTop = [y/100000, x/100000],
	    center = [rightTop[0]/2, rightTop[1]/2],
	    defaultZoom = 16;
	
	var scrollUnit = rightTop[1]/20;
	var map = L.map("imagemap").setView(center, defaultZoom);
	map.setMaxBounds([leftBottom, rightTop]);//写真外にスクロールしない
	map.setMinZoom(16).setMaxZoom(22);	//ズーム倍率範囲設定
	var imageUrl = image,
	    imageBounds = [leftBottom, rightTop];
	L.imageOverlay(imageUrl, imageBounds,
		       {attribution: "東北公益文科大学"}).addTo(map);
	var musium = L.marker([0.00683, 0.00584]).bindPopup(
	    '<h2>一番蚕室</h2>\
<p><a href="https://www.chido.jp/matsugaoka/contents/1_kinenkan.html">\
松ヶ岡開墾記念館</a></p>').addTo(map);
	var silkhouse = L.marker([0.006488, 0.036756]).bindPopup(
	    "<h2>三番蚕室</h2>\n<p>実際に養蚕が行なわれている</p>").addTo(map)
	var eigamura = L.marker([0.007196, 0.067292]).bindPopup(
	    "<h2>五番蚕室</h2>\n<p>庄内映画村資料館</p>").addTo(map);

	// クリックした緯度経度を出すようにしておくと marker 位置が探しやすい
	// ポイントを追加したい場合は次の行を有効化する
	//  map.on("click", function(e) {alert(e.latlng);});

	var mapW = document.getElementById("imagemap").offsetWidth;
	var defaultPanX = mapW/30;
	function panMapLng(pixels) {
	    map.panBy([pixels, 0]);
	}
	function sL() {
	    panMapLng(-defaultPanX);
	}
	function sR() {
	    panMapLng(defaultPanX);
	}
	function C() {
	    map.setView(center, defaultZoom, {animate: true});
	}
	document.getElementById("btnL").addEventListener("click", sL, false);
	document.getElementById("btnC").addEventListener("click", C, false);
	document.getElementById("btnR").addEventListener("click", sR, false);
    }
    document.addEventListener("DOMContentLoaded", init, false);
})();
