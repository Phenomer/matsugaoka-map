#-*- coding: utf-8 -*-

class SvgBuilder:
    def build(self, detector, shape):
        list = []
        list.append({'type':   'rect',
                     'x':      detector.left(),
                     'y':      detector.top(),
                     'width':  detector.right() - detector.left(),
                     'height': detector.bottom() - detector.top()})
        list.append({'type': 'line',
                     'x1':   shape.part(36).x,
                     'y1':   shape.part(36).y,
                     'x2':   shape.part(39).x,
                     'y2':   shape.part(36).y})
        list.append({'type': 'line',
                     'x1':   shape.part(42).x,
                     'y1':   shape.part(42).y,
                     'x2':   shape.part(45).x,
                     'y2':   shape.part(45).y})
        list.append({'type': 'line',
                     'x1':   shape.part(27).x,
                     'y1':   shape.part(27).y,
                     'x2':   shape.part(29).x,
                     'y2':   shape.part(29).y})
        list.append({'type': 'line',
                     'x1':   shape.part(29).x,
                     'y1':   shape.part(29).y,
                     'x2':   shape.part(33).x,
                     'y2':   shape.part(33).y})
        list.append({'type': 'line',
                     'x1':   shape.part(51).x,
                     'y1':   shape.part(51).y,
                     'x2':   shape.part(57).x,
                     'y2':   shape.part(57).y})
        list.append({'type': 'line',
                     'x1':   shape.part(48).x,
                     'y1':   shape.part(48).y,
                     'x2':   shape.part(54).x,
                     'y2':   shape.part(54).y})
        list.append({'type': 'line',
                     'x1':   shape.part(31).x,
                     'y1':   shape.part(31).y,
                     'x2':   shape.part(35).x,
                     'y2':   shape.part(35).y})
        list.append({'type': 'line',
                     'x1':   shape.part(62).x,
                     'y1':   shape.part(62).y,
                     'x2':   shape.part(66).x,
                     'y2':   shape.part(66).y})
        list.append({'type': 'line',
                     'x1':   shape.part(61).x,
                     'y1':   shape.part(61).y,
                     'x2':   shape.part(67).x,
                     'y2':   shape.part(67).y})
        list.append({'type': 'line',
                     'x1':   shape.part(63).x,
                     'y1':   shape.part(63).y,
                     'x2':   shape.part(65).x,
                     'y2':   shape.part(65).y})
        return list
