# これはなに?
顔の左右の向きを大雑把に検知してWebページ上に表示するシステムです。


# つかいかた
## 必要なソフトウェアのインストール

python3とlibdlib、OpenCV、ビルドツールをインストールします。

```
sudo apt install python3 python3-pip libdlib-dev libopencv-dev cmake build-essential v4l-utils
```

## 必要なライブラリのインストール
サーバの動作に必要なライブラリをインストールします。

```
pip3 install --user --upgrade Flask
pip3 install --user --upgrade numpy
pip3 install --user --upgrade dlib
pip3 install --user --upgrade opencv-python
pip3 install --user --upgrade websockets
```

## ハードウェアの準備
Webカメラを接続してください。
複数のWebカメラが存在する場合は、使いたいカメラに合わせてwsface.pyのCAMERA_DEV変数を書き換えてください。
(/dev/video0なら0、/dev/video1なら1)

デバイスの一覧はv4l2-ctlコマンドで調査できます。
```
v4l2-ctl --list-devices
```

また、カメラがサポートしている解像度やパラメータなどを調査することもできます。　
```
v4l2-ctl -d /dev/video0 --list-formats-ext
v4l2-ctl -d /dev/video0 --all
```

## Webサービスの開始
server.shを実行し、ブラウザから http://127.0.0.1:5000 にアクセスしてください。


# 注意点
./staticディレクトリ内に画像ファイルを大量に書き出すので、
フラッシュメモリ上などで利用する場合は注意してください。
(tmpfs上などで動作させることを推奨します。)
