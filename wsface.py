#!/usr/bin/python3
#-*- coding: utf-8 -*-

import asyncio
import websockets
import math
import sys
import os
import json
import dlib
import svgbuilder
import cv2

CAMERA_DEV  = 2
WIDTH       = 320
HEIGHT      = 240

IMG_NAME    = './static/sample.jpg'
SVG_NAME    = './static/sample.svg'
LEARN_DATA  = '/usr/share/dlib/shape_predictor_68_face_landmarks.dat'

video       = cv2.VideoCapture(CAMERA_DEV)
video.set(cv2.CAP_PROP_FRAME_WIDTH,  WIDTH)
video.set(cv2.CAP_PROP_FRAME_HEIGHT, HEIGHT)

detector    = dlib.get_frontal_face_detector()
predictor   = dlib.shape_predictor(LEARN_DATA)
count       = 0

def distance(p1, p2):
    return math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2)

def image_processor():
    global count
    frame = video.read()
    if frame[0] != True:
        print("CAMERA ERROR!!!")
        return {'x': 0, 'y': 0, 'z': 0, 'orientation': 0}
    count += 1
    if count % 10 == 0:
        cv2.imwrite('static/sample.jpg', frame[1])
    dets = detector(frame[1], 1)
    svgb = svgbuilder.SvgBuilder()
    c, x, y = 0, 0, 0
    left, center, right = None, None, None
    svgary = []
    for k, d in enumerate(dets):
        shape = predictor(frame[1], d)
        c += 1
        # 顔の中心(30)
        x += shape.part(30).x
        y += shape.part(30).y
        # 口の左(48), 中央(62), 右(54)
        left   = shape.part(48)
        center = shape.part(62)
        right  = shape.part(54)
        mouse  = distance(shape.part(62), shape.part(66))
        svgary.append(svgb.build(d, shape))

    if (c > 0):
        leftweight = distance(left, center)
        rightweight = distance(center, right)
        orientation = leftweight - rightweight
        xabs, yabs = WIDTH / 2, HEIGHT / 2
        xpos, ypos = (x / c - xabs) / xabs, (y / c - yabs) / yabs
        return {'x': xpos * -1, 'y': ypos, 'z': 0,
                'mouse': mouse, 'orientation': orientation, 'svg': svgary}
    else:
        return {'x': 0, 'y': 0, 'z': 0, 'mouse': 0, 'orientation': 0, 'svg': []}

async def app(websocket, path):
    while True:
        face = json.dumps(image_processor())
        print(face)
        await websocket.send(face)
    
if __name__ == '__main__':
    start_server = websockets.serve(app, "0.0.0.0", 5001,
                                    max_size=10 * 1024 * 1024,
                                    ping_interval=3, ping_timeout=1, close_timeout=1)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
