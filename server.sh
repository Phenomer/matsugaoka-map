#!/bin/bash

wsface_kill(){
  kill ${WSOCK_PID}
  printf "kill %d\n" ${WSOCK_PID}
}

trap wsface_kill SIGINT

python3 wsface.py &
WSOCK_PID=$!
python3 -m flask run --host=0.0.0.0
